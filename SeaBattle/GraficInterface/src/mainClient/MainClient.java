package mainClient;

import javax.swing.JButton;
import javax.swing.JFrame;

import siabattlewindow.GameWindow;

public class MainClient {
	private static int[][] arr = {{9,0,0,0,0,0,0,0,0,0}, 
								  {0,0,0,0,0,0,0,0,0,0}, 
								  {0,0,0,3,3,0,0,1,0,0}, 
								  {0,0,0,0,3,0,0,0,0,0}, 
								  {0,1,1,0,0,0,0,0,0,1}, 
								  {0,0,0,0,0,0,0,0,0,1}, 
								  {3,0,0,0,0,0,0,0,0,0}, 
								  {3,0,0,0,0,1,1,0,0,0}, 
								  {3,0,0,0,0,0,0,0,0,0}, 
								  {0,0,0,0,0,0,0,0,0,8}}; 
	
	public static void main(String[] args) {
	
		JFrame myWindow = new GameWindow("Player2", false, arr);
		myWindow.setVisible(true);
		((GameWindow) myWindow).createClientSock();
	}
}
