package mainServer;

import javax.swing.JButton;
import javax.swing.JFrame;

import siabattlewindow.GameWindow;

public class MainServer {
	private static int[][] arr = {{8,0,0,0,0,0,0,0,0,0}, 
								  {0,0,0,0,0,0,0,0,0,0}, 
								  {0,0,0,3,3,0,0,1,0,0}, 
								  {0,0,0,0,3,0,0,0,0,0}, 
								  {0,1,1,0,0,0,0,0,0,1}, 
								  {0,0,0,0,0,0,0,0,0,1}, 
								  {3,0,0,0,0,0,0,0,0,0}, 
								  {3,0,0,0,0,1,1,0,0,0}, 
								  {3,0,0,0,0,0,0,0,0,0}, 
								  {0,0,0,0,0,0,0,0,0,9}}; 
	
	public static void main(String[] args) {
	
		JFrame myWindow = new GameWindow("Player1", true, arr);
		myWindow.setVisible(true);
		((GameWindow) myWindow).createServerSock();
	}
}
