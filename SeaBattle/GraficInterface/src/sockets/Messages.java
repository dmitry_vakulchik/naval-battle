package sockets;

import java.util.ArrayList;

public class Messages {
	
	private ArrayList<Message> messages;
	
	int nextMsg = -1;
	
	public Messages () { 
		this.messages = new ArrayList<Message>();
	}

	public ArrayList<Message> getMessages() {
		return messages;
	}
	
	public void addMessage(Message message) {
		messages.add(message);
		nextMsg++;
	}
	
	public Message getLastMessage() {
		return messages.get(nextMsg);
	}
}
