 package sockets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class MySocket {
	
	ServerSocket serverSocket;
	Socket clientSocket;
	
	DataInputStream dataIn;
	DataOutputStream dataOut;
	
	InputStream inSteam;
	OutputStream outStream;
	
	public void createClientSock(String ip, int port) {
		
		InetAddress address;
		
		try {
			address = InetAddress.getByName(ip);
			clientSocket = new Socket(address, port);
			this.createStreams();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void createStreams() {
		try {
			inSteam = clientSocket.getInputStream();
			dataIn = new DataInputStream(inSteam);
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		try {
			outStream = clientSocket.getOutputStream();
			dataOut = new DataOutputStream(outStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}
	
	public void close() {
		try {
			dataIn.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
		
		try {
			dataOut.flush();
			dataOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			inSteam.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			outStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void closeClientSock() {
		try {
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void closeServerSock() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void createServerSock(int port) {
		
		try {
			serverSocket = new ServerSocket(port);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void waitClientConnection() {
		try {
			clientSocket = serverSocket.accept();
			this.createStreams();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendMessage(Message message) {
		
		String convertedMsg = message.toString();
		
		try {
			dataOut.writeUTF(convertedMsg);			
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	public Message recvMessage() {
		
		String inputMsg = null;
		
		try {
			inputMsg =  dataIn.readUTF();			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Message message = new Message(inputMsg);
		
		return message;
	}
}
