package seabattle;

public class Game {
	
    private int[][] polygon;
    private int count;

    public Game(int[][] polygon)
    {
    	this.count = 0;
        this.polygon = new int[12][12];
        for (int i = 0; i < 12; i++)
            for (int j = 0; j < 12; j++)
            {
                this.polygon[i][j] = -1;
            }
        for (int i = 1; i < 11; i++)
            for (int j = 1; j < 11; j++)
            {
                this.polygon[i][j] = polygon[i - 1][j - 1];
            }
    }

    public boolean Hit(int y, int x)
    {
        x++;
        y++;
        if (polygon[x][y] == 1)
        {
            polygon[x][y] = -1;
            seaCord(x,y);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void seaCord(int x, int y)
    {
        boolean isTrue = false;
        int[][] c = {{ 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 }};
        for (int i = 0; i < 4; i++) 
        {
        	if (polygon[x + c[i][0]][y + c[i][1]] == 1)
                isTrue = true;
        }
        if (!isTrue) 
        	this.count++;
    }

    public boolean isEnd()
    {
    	boolean end = this.count == 10 ? true : false;
        return end;
    }
	
	
}
