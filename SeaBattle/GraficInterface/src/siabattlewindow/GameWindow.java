package siabattlewindow;

import java.awt.*;

import javax.swing.*;

import decorator.Ship;
import decorator.Ship1;
import decorator.Ship2;
import decorator.ShipCount;
import decorator.ShipHelpoint;
import decorator.ShipPosition;
import factory.metchod.Ammunition;
import factory.metchod.ConcreteCreatorShell;
import factory.metchod.ConcreteCreatorTorpedo;
import factory.metchod.Torpedo;
import seabattle.Game;
import sockets.Message;
import sockets.Messages;
import sockets.MySocket;
import sockets.MySocketThread;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class GameWindow extends JFrame implements KeyListener {
	
	private int[][] arr, arrCopy;
	public int x,y;
	private Message message;
	private Game game;
	private int count = 0;
	private boolean isServer;
	private String msg;
	private Ship ship;
	private Ship ship1;
	private Image img1, img2, img3, img4, img5, img6;
	private String playerName = "";
	
	private ArrayList<Ammunition> ammunition = new ArrayList<Ammunition>();
	private ArrayList<Ammunition> ammunition1 = new ArrayList<Ammunition>();
	
	public GameWindow(String msg, boolean isServer, int[][] arr) {
		super("Sea Battle " + msg );
		this.playerName = msg;
		this.msg = msg;
		game = new Game(arr);
		this.arr = arr;
		this.arrCopy = new int[10][10];
		for (int j=0; j<10; j++) {			
			for (int i=0; i<10; i++) {
				arrCopy[j][i] = arr[j][i];
			}
		}
		this.isServer=isServer;
		setBounds(100, 100, 800, 600);
		setResizable(false);
		setFocusable(true);
		addKeyListener(this);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
		if (isServer) {
			ship = new Ship1(20, 1, 1, 1, 50);
			ship1 = new Ship2(20, 1, 10, 10, 50);
			CreateWindows();
		} else {
			ship = new Ship2(20, 1, 10, 10, 50);
			ship1 = new Ship1(20, 1, 1, 1, 50);
			CreateWindows();
		}
		loadImage("src/ship1.jpg", "src/ship2.jpg", "src/skala.jpg", "src/mel.jpg", "src/torpedo.jpg", "src/shell.jpg");
	}
	
	private int GetCountTorpedo(ArrayList<Ammunition> ammunition) {
		int count = 0;
		if (ammunition == null) 
			return count;
		
		for (int i = 0; i< ammunition.size(); i++) {
			if (ammunition.get(i).getName() == "torpedo")
				count ++;
		}
		
		return count;
	}
	
	private int GetCountShell(ArrayList<Ammunition> ammunition) {
		int count = 0;
		if (ammunition == null) 
			return count;
		
		for (int i = 0; i< ammunition.size(); i++) {
			if (ammunition.get(i).getName() == "shell")
				count ++;
		}
		
		return count;
	}

	
	public void loadImage(String src1, String src2, String src3, String src4, String src5, String src6) {
	    img1 = new ImageIcon(src1).getImage();
	    img2 = new ImageIcon(src2).getImage();
	    img3 = new ImageIcon(src3).getImage();
	    img4 = new ImageIcon(src4).getImage();
	    img5 = new ImageIcon(src5).getImage();
	    img6 = new ImageIcon(src6).getImage();
	}
	
	private JPanel jPanel1 = new JPanel();
	private JPanel jPanel2 = new JPanel();
	private Container container = this.getContentPane();	
	private JComponent component = new JComponent() { };
	
	private void CreateWindows() {
		component.paint(getGraphics());
	    container.setLayout(new GridLayout(1,3));
	    container.add(jPanel1);
	    container.add(jPanel2);
	}
	
	public void drawIcon(Image img, int x, int y) {
	    Graphics g = getGraphics();
	    g.drawImage(img, x, y, 37, 37, null);
	}

	public void painInfo(Graphics g) {
		g.clearRect(50, 215, 100, 300);
		g.setColor(Color.blue);
		g.drawLine(50, 220, 150, 220);
		g.setColor(Color.black);
		g.drawString(playerName, 50, 250);
		g.drawString("��������: " + ship.getHelpoint(), 50, 270);
		g.drawString("����: " + ship.getCount(), 50, 290);
		g.drawString("�������: " + GetCountTorpedo(ammunition), 50, 310);
		g.drawString("�������: " + GetCountShell(ammunition), 50, 330);
		g.setColor(Color.red);
		g.drawLine(50, 350, 150, 350);
		g.setColor(Color.black);
		g.drawString("���������:", 50, 380);
		g.drawString("��������: " + ship1.getHelpoint(), 50, 400);
		g.drawString("����: " + ship1.getCount(), 50, 420);
		g.drawString("�������: " + GetCountTorpedo(ammunition1), 50, 440);
		g.drawString("�������: " + GetCountShell(ammunition1), 50, 460);
	}
	
	public Ship BuyTorpedo(Ship ship, ArrayList<Ammunition> ammunitions) {
		ship = new ShipCount(ship, ship.getHelpoint(), ship.getSpeed(), ship.getPositionX(), ship.getPositionY(), ship.getCount());
		if (ship.ChangeCount(10)) {
			ammunitions.add(new ConcreteCreatorTorpedo().FactoryMethod());
		}
		return ship;
	}
	
	public Ship BuyShell(Ship ship, ArrayList<Ammunition> ammunitions) {
		ship = new ShipCount(ship, ship.getHelpoint(), ship.getSpeed(), ship.getPositionX(), ship.getPositionY(), ship.getCount());
		if (ship.ChangeCount(5)) {
			ammunitions.add(new ConcreteCreatorShell().FactoryMethod());
		}
		return ship;
	}
	
	public void paint(Graphics g){
		painInfo(g);
		g.setColor(Color.black);
		g.drawString("����������: ", 50, 50);
		g.drawString("�����: w", 70, 70);
		g.drawString("����: s", 70, 90);
		g.drawString("�����: a", 70, 110);
		g.drawString("������: d", 70, 130);
		g.drawString("���������� ��������: q(�����), e(����)", 70, 150);
		g.drawString("��������� ��������: z(�����), x(����)", 70, 170);
		g.drawString("������ �������(���� 10): 1", 70, 190);
		g.drawString("������ ������(���� 5): 2", 70, 210);	
		g.setColor(Color.black);
		for (int j=1; j<12; j++) {
			g.drawLine(300 + 40 * j, 40, 300 + 40 * j,  440);
		}
		for (int j=1; j<12; j++) {
			g.drawLine(340, 40 * j, 740,  40 * j);
		}
		
		for (int j=1; j<11; j++) {			
			for (int i=1; i<11; i++) {
				if (arr[i-1][j-1] == 1) {
					drawIcon(img3, 302 + 40 * i, 2 + 40 * j);
				}
				if (arr[i-1][j-1] == 9) {
					drawIcon(img2, 302 + 40 * i, 2 + 40 * j);
				}
				if (arr[i-1][j-1] == 8) {
					drawIcon(img1, 302 + 40 * i, 2 + 40 * j);
				}
				if (arr[i-1][j-1] == 3) {
					drawIcon(img4, 302 + 40 * i, 2 + 40 * j);
				}
				if (arr[i-1][j-1] == 0) {
					g.setColor(Color.white);
					g.fillRect(302 + 40 * i, 2 + 40 * j, 37, 37);
				}
			}
		}
	}
	
	public void setGo(Graphics g, int x, int y) {
		int xn, yn;
		x = x * ship.getSpeed();
		y = y * ship.getSpeed();
		xn = ship.getPositionX();
		yn = ship.getPositionY();
		if ((xn + x <= 10 && xn + x >= 1) && 
			(yn + y <= 10 && yn + y >= 1) &&
				(arr[xn + x - 1][yn + y - 1] == 0 || arr[xn + x - 1][yn + y - 1] == 3)) {
			
			
			if (arrCopy[xn - 1][yn - 1] == 3) {
				drawIcon(img4, 302 + 40 * xn, 2 + 40 * yn);
			} else {
				g.setColor(Color.white);
				g.fillRect(302 + 40 * xn, 2 + 40 * yn, 37, 37);
			}
			arr[xn - 1][yn - 1] = 0;
			xn += x;
			yn += y;
			drawIcon(img1, 302 + 40 * xn, 2 + 40 * yn);
			arr[xn - 1][yn - 1] = 8;
			ship = new ShipPosition(ship, ship.getHelpoint(), ship.getSpeed(), ship.getPositionX(), ship.getPositionY(), ship.getCount());
			ship.ChangePosition(x, y);
			String str = "go " + x + " " + y;
			message = new Message(str);
			mySocket.sendMessage(message);
		}
	}
	
	public void getGo(Message message) {
 		String str = message.toString();
		String arrStr[];
		if (str.indexOf("go") != -1) {
			arrStr = str.split(" ");
			int x = Integer.parseInt(arrStr[1]);
			int y = Integer.parseInt(arrStr[2]);
			Graphics g = getGraphics();
			int xn, yn;
			x = x * ship1.getSpeed();
			y = y * ship1.getSpeed();
			xn = ship1.getPositionX();
			yn = ship1.getPositionY();
			
			if ((xn + x <= 10 && xn + x >= 1) && 
					(yn + y <= 10 && yn + y >= 1) &&
						(arr[xn + x - 1][yn + y - 1] == 0 || arr[xn + x - 1][yn + y - 1] == 3)) {
				
				if (arrCopy[xn - 1][yn - 1] == 3) {
					drawIcon(img4, 302 + 40 * xn, 2 + 40 * yn);
				} else {
					g.setColor(Color.white);
					g.fillRect(302 + 40 * xn, 2 + 40 * yn, 37, 37);
				}
				arr[xn - 1][yn - 1] = 0;
				xn += x;
				yn += y;
				drawIcon(img2, 302 + 40 * xn, 2 + 40 * yn);
				arr[xn - 1][yn - 1] = 9;
				ship1 = new ShipPosition(ship1, ship1.getHelpoint(), ship1.getSpeed(), ship1.getPositionX(), ship1.getPositionY(), ship1.getCount());
				ship1.ChangePosition(x, y);
			}
		}
		if (str.indexOf("amunition") != -1) {
			if (str.indexOf("torpedo") != -1) {
				ship1 = BuyTorpedo(ship1, ammunition1);
			}
			if (str.indexOf("shell") != -1) {
				ship1 = BuyShell(ship1, ammunition1);
			}
			painInfo(getGraphics());
		}
		
		if (str.indexOf("fire") != -1) {
			if (str.indexOf("torpedo") != -1) {
				arrStr = str.split(" ");
				FireTorpedo(ammunition1, ship1, Integer.parseInt(arrStr[2]), 1);
			}
			if (str.indexOf("shell") != -1) {
				arrStr = str.split(" ");
				FireSell(ammunition1, ship1, Integer.parseInt(arrStr[2]), 1);
			}
			painInfo(getGraphics());
		}
		
		if (str.indexOf("helpoint") != -1) {
			arrStr = str.split(" ");
			ship1 = new ShipHelpoint(ship1, ship1.getHelpoint(), ship1.getSpeed(), ship1.getPositionX(), ship1.getPositionY(), ship1.getCount());
			
			ship1.ChangeHelpoint(Integer.parseInt(arrStr[1]));
			if (ship1.getHelpoint() <= 0) {
				JOptionPane.showMessageDialog(this, "�� ��������!");
				try {
					Thread.sleep(800);
					System.exit(0);
				} catch (InterruptedException e) {
					System.exit(0);
				}
			}
			painInfo(getGraphics());
		}
	}
	
	private MySocket mySocket = new MySocket();
	private MySocketThread mySocketThread;
	private Messages messages;
	
	public void createServerSock() {
		System.out.print("Server started\n");		
		mySocket.createServerSock(555);
		System.out.print("Wait client connection\n");
		mySocket.waitClientConnection();
		System.out.print("Client connected\n");	
		messages = new Messages();
		mySocketThread = new MySocketThread(mySocket, messages, this);
		mySocketThread.start();	
	}
	
	public void removeServerSock() {
		mySocketThread.setActive(false);
		try {
			
			mySocketThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		mySocket.close();
		mySocket.closeServerSock();
		System.out.print("Server finished\n");
	}
	
	public void createClientSock() {
		System.out.print("Client started\n");
		mySocket.createClientSock("127.0.0.1", 555);
		System.out.print("Connection success full\n");
		messages = new Messages();
		mySocketThread = new MySocketThread(mySocket, messages, this);
		mySocketThread.start();
	}
	
	public void removeClientSock() {
		mySocketThread.setActive(false);
		try {
			
			mySocketThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
		mySocketThread.setActive(false);
		try {
			
			mySocketThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
		mySocket.close();
		mySocket.closeClientSock();
		System.out.print("Client finished\n");
	}
	
	public void removeSock() {
		System.out.print("Close... Wait...");
		if (isServer) {
			removeServerSock();
		} else {
			removeClientSock();
		}
	}

	@Override
    public void keyPressed(KeyEvent e) {
		switch (e.getKeyChar()) {
		case 'w':
			setGo(getGraphics(), 0, -1);
			break;
		case 's':
			setGo(getGraphics(), 0, 1);
			break;
		case 'a':
			setGo(getGraphics(), -1, 0);
			break;
		case 'd':
			setGo(getGraphics(), 1, 0);
			break;
		case '1':
			ship = BuyTorpedo(ship, ammunition);
			message = new Message("amunition torpedo");
			mySocket.sendMessage(message);
			painInfo(getGraphics());
			break;
		case '2':
			ship = BuyShell(ship, ammunition);
			message = new Message("amunition shell");
			mySocket.sendMessage(message);
			painInfo(getGraphics());
			break;
		case 'q':
			FireTorpedo(ammunition, ship, -1, 0);
			break;
		case 'e':
			FireTorpedo(ammunition, ship, 1, 0);
			break;
		case 'x':
			FireSell(ammunition, ship, -1, 0);
			break;
		case 'z':
			FireSell(ammunition, ship, 1, 0);
			break;
		default:
			break;
		}
    }
	
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void FireSell(ArrayList<Ammunition> ammunitions, Ship ship, int shag, int whu) {
		if (GetCountShell(ammunitions) > 0) {
			if (whu == 0) {
				message = new Message("fire shell " + shag);
				mySocket.sendMessage(message);
			}
			Ammunition shell = null;
			for (int i = 0; i< ammunitions.size(); i++) {
				if (ammunitions.get(i).getName().equals("shell") && shell == null) {
		    	   try {
		    		   shell = ammunitions.get(i).clone();
					} catch (CloneNotSupportedException e) {
						e.printStackTrace();
					}
			    	   ammunitions.remove(ammunitions.get(i));
				}
			}
			try {
				PaintAmm(shell, ship.getPositionX(), ship.getPositionY(), shag, getGraphics(), img6, whu);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			painInfo(getGraphics());
		}
	}
	
	public void FireTorpedo(ArrayList<Ammunition> ammunitions, Ship ship, int shag, int whu) {
		if (GetCountTorpedo(ammunitions) > 0) {
			if (whu == 0) {
				message = new Message("fire torpedo " + shag);
				mySocket.sendMessage(message);
			}
			Ammunition torpedo = null;
			for (int i = 0; i< ammunitions.size(); i++) {
				if (ammunitions.get(i).getName().equals("torpedo") && torpedo == null) {
		    	   try {
		    		   torpedo = ammunitions.get(i).clone();
					} catch (CloneNotSupportedException e) {
						e.printStackTrace();
					}
		    	    ammunitions.remove(ammunitions.get(i));
				}
			}
			try {
				PaintAmm(torpedo, ship.getPositionX(), ship.getPositionY(), shag, getGraphics(), img5, whu);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			painInfo(getGraphics());
		}
	}
	
	public void PaintAmm(Ammunition amm, int x0, int y0, int shag, Graphics g, Image img, int whu) throws InterruptedException {
		int counter = 0;
		System.out.println("whu:" + whu);
		boolean flag = true;
		while (flag) {
			y0 = y0 + shag;
			if (y0 >= 1 && y0 <= 10) {
				if (arr[x0 - 1][y0 - 1] == 0) {
					drawIcon(img, 302 + 40 * x0, 2 + 40 * y0);
					Thread.sleep(500 * amm.getSpeed());
					g.setColor(Color.white);
					g.fillRect(302 + 40 * x0, 2 + 40 * y0, 37, 37);
				} 
				if (arr[x0 - 1][y0 - 1] == 3) {
					drawIcon(img, 302 + 40 * x0, 2 + 40 * y0);
					Thread.sleep(700 * amm.getSpeed());
					drawIcon(img4, 302 + 40 * x0, 2 + 40 * y0);
				}
				if (arr[x0 - 1][y0 - 1] == 1) {
					flag = false;
				}
				if (arr[x0 - 1][y0 - 1] == 8 && whu == 1) {
					GetFire(amm, 0, x0, y0);
					flag = false;
				}
				if (arr[x0 - 1][y0 - 1] == 9 && whu == 0) {
					flag = false;
				}
			}
			counter++;
			if (amm.getLength() + 1 <= counter) {
				if (whu == 1 && flag) { 
					GetFire(amm, 1, x0, y0);
				}
				flag = false;
			}
		}
	}
	
	public void GetFire(Ammunition amm, int endFlag, int x0, int y0) {
		if (endFlag == 0) {
			checkHelpoint(amm);
		} else {
			if (checkArray(y0, x0) && (arr[x0 - 1][y0 - 1] == 8)) { 
				checkHelpoint(amm);
			}
			int radius = amm.getRadius();
			while (radius > 0) {
				if (checkArray(y0, x0 + radius) && (arr[x0 - 1  + radius][y0 - 1] == 8)) { 
					checkHelpoint(amm);
				}
				if (checkArray(y0, x0 - radius) && (arr[x0 - 1 - radius][y0 - 1] == 8)) { 
					checkHelpoint(amm);
				}
				if (checkArray(y0 + radius, x0) && (arr[x0 - 1][y0 - 1 + radius] == 8)) { 
					checkHelpoint(amm);
				}
				if (checkArray(y0 - radius, x0) && (arr[x0 - 1][y0 - 1 - radius] == 8)) { 
					checkHelpoint(amm);
				}
				radius--;
			}
			
		}
	}
	
	private void checkHelpoint(Ammunition amm) {
		ship = new ShipHelpoint(ship, ship.getHelpoint(), ship.getSpeed(), ship.getPositionX(), ship.getPositionY(), ship.getCount());
		ship.ChangeHelpoint(amm.getForce());
		message = new Message("helpoint " + amm.getForce());
		mySocket.sendMessage(message);
		if (ship.getHelpoint() <= 0) {
			JOptionPane.showMessageDialog(this, "�� ���������!");
			try {
				Thread.sleep(800);
				System.exit(0);
			} catch (InterruptedException e) {
				System.exit(0);
			}
			
		}
	}
	
	private boolean checkArray(int y0, int x0) {
		if (y0 <= 10 && y0 >= 1 && x0 <= 10 && x0 >= 1)
			return true;
		else
			return false;
	}
}

