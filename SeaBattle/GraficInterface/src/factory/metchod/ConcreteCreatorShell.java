package factory.metchod;

public class ConcreteCreatorShell extends Creator{

	@Override
	public Ammunition FactoryMethod() {
		return new Shell(2, -3, "shell", 1, 2);
	}

}
