package factory.metchod;

public abstract class Ammunition implements Cloneable{
	private int length;
	private int force;
	private String name;
	private int speed;
	private int radius;
	
	public Ammunition(int length, int force, String name, int speed, int radius)
    {
        this.length = length;
        this.force = force;
        this.name = name;
        this.speed = speed;
        this.radius = radius;
    }
	
	abstract public Creator Create();

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getForce() {
		return force;
	}

	public void setForce(int force) {
		this.force = force;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Ammunition clone() throws CloneNotSupportedException{
        return (Ammunition) super.clone();
    }
}
