package factory.metchod;

public class ConcreteCreatorTorpedo extends Creator{

	@Override
	public Ammunition FactoryMethod() {
		return new Torpedo(2, -5, "torpedo", 1, 3);
	}
}
