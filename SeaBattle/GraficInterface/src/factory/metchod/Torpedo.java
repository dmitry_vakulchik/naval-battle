package factory.metchod;

public class Torpedo extends Ammunition{
	public Torpedo(int length, int force, String name, int speed, int radius) {
		super(length, force, name, speed, radius);
	}
	
	@Override
	public Creator Create() {
		return new ConcreteCreatorTorpedo();
	}
}
