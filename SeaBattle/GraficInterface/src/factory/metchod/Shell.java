package factory.metchod;

public class Shell extends Ammunition{

	public Shell(int length, int force, String name, int speed, int radius) {
		super(length, force, name, speed, radius);
	}

	@Override
	public Creator Create() {
		return new ConcreteCreatorShell();
	}
}
