package decorator;

public abstract class ShipDecorator extends Ship{

	protected Ship ship;
	public ShipDecorator(Ship ship, int helpoint, int speed, int positionX, int positionY, int count) {
		super(helpoint, speed, positionX, positionY, count);
		this.ship = ship;
	}
}
